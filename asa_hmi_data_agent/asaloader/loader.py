from asa_hmi_data_agent.listport import getAvailableSerialPorts
from asa_hmi_data_agent.ui.ui_widget_asa_loader import Ui_WidgetAsaLoader
from asa_hmi_data_agent.util import ADTPATH
from asa_hmi_data_agent.qt_resource import qt_resource

from PyQt5.QtCore import pyqtSlot, QThread, pyqtSignal, QObject
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtGui import QIcon

from time import sleep
from asaloader import ihex, loader
import asaloader
import datetime
import serial
import math
import os


# ---- class LoaderThread Start -------------------------------------------------
class LoaderThread(QThread):
    sigState = pyqtSignal(bool, int, str)
    sigChkAsaDevice = pyqtSignal(bool)
    sigStart = pyqtSignal(int)
    sigStepDone = pyqtSignal(int)
    sigEnd = pyqtSignal(bool)
    sigUpdateInfo = pyqtSignal(str, float, float)
    sigException = pyqtSignal(object)
    sigStage = pyqtSignal(int)

    def __init__(self):
        QThread.__init__(self)
        self.cmd = str()

    def setParameter(
        self, port, dev, flash_file, eeprom_file, is_go_app, go_app_delay
    ):
        self.port = port
        self.dev = dev

        if flash_file == '':
            self.is_prog_flash = False
        else:
            self.is_prog_flash = True

        if eeprom_file == '':
            self.is_prog_eeprom = False
        else:
            self.is_prog_eeprom = True

        self.is_go_app = is_go_app
        self.flash_file = flash_file
        self.eeprom_file = eeprom_file
        self.go_app_delay = go_app_delay
        self.max = 0

    def run(self):
        ser = serial.Serial()
        ser.port = self.port
        ser.timeout = 1
        ser.baudrate = 115200
        ser.open()
        self.ser = ser
        try:
            load = asaloader.loader.Loader(
                ser=self.ser,
                device_type=self.dev,
                is_flash_prog=self.is_prog_flash,
                is_eeprom_prog=self.is_prog_eeprom,
                is_go_app=self.is_go_app,
                flash_file=self.flash_file,
                eeprom_file=self.flash_file,
                go_app_delay=self.go_app_delay,
            )
        except Exception as e:
            self.sigException.emit(e)
            self.ser.close()
            return
        self.sigStart.emit(load.total_steps-1)
        
        dev_name = asaloader.device.device_list[load.device_type]['name']
        flash_size = load.flash_size / 1024
        prog_time = load.prog_time

        self.sigUpdateInfo.emit(dev_name, flash_size, prog_time)

        for i in range(load.total_steps):
            try:
                load.do_step()
            except UnicodeDecodeError as e:
                self.sigException.emit(e)
                self.ser.close()
                return

            if i == 0:
                self.sigStepDone.emit(i)
                self.sigChkAsaDevice.emit(True)
            elif i == load.total_steps-1:
                self.sigStepDone.emit(i)
                self.sigEnd.emit(True)
                self.ser.close()
            else:
                self.sigStepDone.emit(i)
                self.sigStage.emit(load.stage)
        debugLog('LoaderThread programming is done')

    def stop(self):
        self.terminate()


class AsaLoader(QObject):
    sigChangeWindowTitle = pyqtSignal(bool, str)
    sigSerialPortCheck = pyqtSignal(bool, str)

    # ---- __init__ start ------------------------------------------------------
    def __init__(self, widget):
        super(AsaLoader, self).__init__()
        self.ui = Ui_WidgetAsaLoader()
        self.ui.setupUi(widget)
        self.ui.progressBar.setValue(0)

        # ---- Thread Init start -----------------------------------------------
        self.loaderThread = LoaderThread()
        self.loaderThread.sigChkAsaDevice[bool].connect(
            self.updateStatus_isAsaDevice)
        self.loaderThread.sigStart[int].connect(self.initProgressbar)
        self.loaderThread.sigStepDone[int].connect(self.setProgressbar)
        self.loaderThread.sigEnd[bool].connect(self.updateStatus_end)
        # self.loaderThread.sigGetSerialException.connect(self.updateStatus_serialException)
        self.loaderThread.finished.connect(
            lambda: self.sigSerialPortCheck.emit(
                False, self.ui.comboBox_selectPort.currentText()
            )
        )
        self.loaderThread.sigUpdateInfo.connect(self.updateUiProgInfo)
        self.loaderThread.sigException.connect(self.lt_exceptions_handler)
        self.loaderThread.sigStage.connect(self.updateUiStage)
        # ---- Thread Init end -------------------------------------------------

        # ---- Basic Group start ----------------------------------------------
        self.serial_updatePortlist()
        self.ui.pushButton_updatePortList.clicked.connect(
            self.serial_updatePortlist)
        self.ui.checkBox_goApp.toggled.connect(self.updateUiGoAppDelay)
        self.ui.pushButton_updatePortList.setIcon(QIcon(':/img/refresh.png'))
        # ---- Basic Group end ------------------------------------------------

        # ---- Basic Functions Group start -------------------------------------
        self.ui.pushButton_selectFile.clicked.connect(self.chooseProgFile)
        self.ui.pushButton_selectFile.setIcon(QIcon(':/img/folder.png'))
        self.ui.pushButton_startProg.clicked.connect(self.startLoad)
        self.ui.pushButton_startProg.setIcon(QIcon(':/img/play.png'))
        self.ui.pushButton_stopProg.clicked.connect(self.stopLoading)
        self.ui.pushButton_stopProg.setIcon(QIcon(':/img/stop.png'))
        # ---- Basic Functions Group end ---------------------------------------

        # ---- Special Functions Group start -----------------------------------
        self.ui.pushButton_progStk500.clicked.connect(self.startLoadStk500)
        self.ui.pushButton_progM128.clicked.connect(self.startLoadM128TestProg)
        self.ui.pushButton_progM3.clicked.connect(self.startLoadM3TestProg)
        # ---- Special Functions Group end -------------------------------------

        self.updateUiGoAppDelay()
        
        self.ui.comboBox_selectDevice.clear()
        for dev in asaloader.device.device_list:
            self.ui.comboBox_selectDevice.addItem(dev['name'])

    # ---- LoaderThread Related functions start --------------------------------
    def initProgressbar(self, max):
        self.ui.progressBar.setRange(0, max)

    def setProgressbar(self, val):
        self.ui.progressBar.setValue(val)

    def updateStatus_isAsaDevice(self, isAsaDevice):
        if isAsaDevice:
            self.ui.label_statusContent.setText(u"確認裝置成功，開始燒錄")
        else:
            self.ui.label_statusContent.setText(u"請確認裝置連接，並按下reset按鈕")

    def updateStatus_end(self, isOK):
        if isOK:
            self.ui.label_statusContent.setText(u"燒錄成功！")
        else:
            self.ui.label_statusContent.setText(u"燒錄失敗！")

    def updateStatus_serialException(self):
        self.ui.label_statusContent.setText(u"與串列埠失去連線，請檢察USB線是否有連接好")

    def lt_exceptions_handler(self, e):
        """handle exctption throw by lt(LoaderThread)
        """
        exception_type = type(e)
        debugLog(f"LoaderThread throw exception:")
        debugLog(f"  type is {exception_type}")
        debugLog(f"  data is {e}")
        if exception_type is asaloader.exceptions.ComuError:
            self.ui.label_statusContent.setText(u"燒錄失敗！")
        elif exception_type is asaloader.exceptions.CheckDeviceError:
            set_dev_name = asaloader.device.device_list[e.in_dev]['name']
            real_dev_name = asaloader.device.device_list[e.real_dev]['name']
            s = u"裝置不吻合\n設定的裝置為'{0}'\n偵測到的裝置為'{1}'".format(
                set_dev_name, real_dev_name)
            self.ui.label_statusContent.setText(s)
        else:
            self.ui.label_statusContent.setText(u"燒錄失敗！")

    def updateUiStage(self, stage):
        if stage == 1:
            self.ui.label_statusContent.setText(u"燒錄Flash中...")
        elif stage == 2:
            self.ui.label_statusContent.setText(u"燒錄EEPROM中...")

    def updateUiProgInfo(self, dev_name, flash_size, prog_time):
        self.ui.label_deviceContent.setText(dev_name)
        self.ui.label_programSizeContent.setText("{:.2f} KB".format(flash_size))
        self.ui.label_etcContent.setText("{:.2f} s".format(prog_time))

    def setupLoaderThreadParameter(self):
        """依據當前UI的設定，將數值傳入LoaderThread
        """
        port = self.ui.comboBox_selectPort.currentText()
        if port == '':
            self.ui.label_statusContent.setText(u"未選擇串列埠")
            return

        dev = self.ui.comboBox_selectDevice.currentIndex()

        flash_file = self.ui.lineEdit_selectFile.text()
        if flash_file == '':
            self.ui.label_statusContent.setText(u"未選擇燒錄檔案")
            return
        elif self.checkIsHexFile(flash_file) is False:
            self.ui.label_statusContent.setText(u"檔案格式錯誤，請重新選擇燒錄檔案")
            return

        # TODO: EEPROM 燒錄為未來愈開發的功能
        eeprom_file = ''

        is_go_app = self.ui.checkBox_goApp.isChecked()
        go_app_delay = 1000
        if is_go_app:
            try:
                go_app_delay = int(self.ui.lineEdit_goAppDelay.text())
            except Exception:
                self.ui.label_statusContent.setText(u"執行應用延遲必須為數字")

            if go_app_delay < 50 or go_app_delay > 30000:
                self.ui.label_statusContent.setText(u"執行應用延遲範圍須在50~30000之間")

        self.loaderThread.setParameter(
            port, dev, flash_file, eeprom_file, is_go_app, go_app_delay)
    # ---- LoaderThread Related functions end ----------------------------------

    # ---- Serial Related functions start --------------------------------------
    def serial_updatePortlist(self):
        """Update port list in comboBox_selectPort."""
        availablePorts = getAvailableSerialPorts()
        debugLog('Update ports: ' + str(availablePorts))
        self.ui.comboBox_selectPort.clear()
        for port in availablePorts:
            self.ui.comboBox_selectPort.addItem(port)
    # ---- Serial Related functions end ----------------------------------------

    # ---- Basic Functions Group start -----------------------------------------
    def chooseProgFile(self):
        name, _ = QFileDialog.getOpenFileName(
            filter='All Files (*);;Hex Files (*.hex)',
            initialFilter='Hex Files (*.hex)'
        )
        if name is not '':
            self.ui.lineEdit_selectFile.setText(name)
            self.checkIsHexFile(name)

    def checkIsHexFile(self, filename):
        try:
            hexbin = asaloader.ihex.parse(filename)
        except (NameError, UnicodeDecodeError):
            self.ui.label_programSizeContent.setText(u"非HEX檔案格式，請重新選擇檔案")
            self.ui.label_etcContent.setText('-')
            return False
        else:
            return True

    def startLoad(self):
        if self.loaderThread.isRunning():
            return
        
        port = self.ui.comboBox_selectPort.currentText()
        self.setupLoaderThreadParameter()
        self.sigSerialPortCheck.emit(True, port)
        self.ui.label_statusContent.setText(u"確認裝置中...")
        self.loaderThread.start()

    def stopLoading(self):
        if self.loaderThread.isRunning():
            self.loaderThread.stop()
            self.sigSerialPortCheck.emit(
                False, self.ui.comboBox_selectPort.currentText())
            self.ui.label_statusContent.setText(u"已強制終止")
    # ---- Basic Functions Group end -------------------------------------------

    # ---- Special Functions Group start ---------------------------------------
    def startLoadStk500(self):
        hexfile = os.path.join(ADTPATH, 'tools\\m128_stk500.hex')
        self.ui.lineEdit_selectFile.setText(hexfile)
        self.startLoad()

    def startLoadM128TestProg(self):
        hexfile = os.path.join(ADTPATH, 'tools\\m128_hello_world.hex')
        self.ui.lineEdit_selectFile.setText(hexfile)
        self.startLoad()

    def startLoadM3TestProg(self):
        hexfile = os.path.join(ADTPATH, 'tools\\m3_hello_world.hex')
        self.ui.lineEdit_selectFile.setText(hexfile)
        self.startLoad()
    # ---- Special Functions Group end -----------------------------------------

    def updateUiGoAppDelay(self):
        if self.ui.checkBox_goApp.isChecked():
            self.ui.lineEdit_goAppDelay.setEnabled(True)
            self.ui.label_goAppDelayTitle.setEnabled(True)
            self.ui.label_goAppDelayUnit.setEnabled(True)
        else:
            self.ui.lineEdit_goAppDelay.setDisabled(True)
            self.ui.label_goAppDelayTitle.setDisabled(True)
            self.ui.label_goAppDelayUnit.setDisabled(True)

def debugLog(msg):
    s = '[{}] loader   log: {}'.format(
        datetime.datetime.now().strftime("%H:%M:%S"),
        msg
    )
    print(s)
