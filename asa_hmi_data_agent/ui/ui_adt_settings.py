# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ui/adt_settings.ui'
#
# Created by: PyQt5 UI code generator 5.14.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWidgetAdtSettings(object):
    def setupUi(self, MainWidgetAdtSettings):
        MainWidgetAdtSettings.setObjectName("MainWidgetAdtSettings")
        MainWidgetAdtSettings.resize(900, 500)
        self.horizontalLayout = QtWidgets.QHBoxLayout(MainWidgetAdtSettings)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.gridLayout_2 = QtWidgets.QGridLayout()
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.label_termNum = QtWidgets.QLabel(MainWidgetAdtSettings)
        self.label_termNum.setObjectName("label_termNum")
        self.gridLayout_2.addWidget(self.label_termNum, 0, 0, 1, 1)
        self.comboBox_termNum = QtWidgets.QComboBox(MainWidgetAdtSettings)
        self.comboBox_termNum.setObjectName("comboBox_termNum")
        self.comboBox_termNum.addItem("")
        self.comboBox_termNum.addItem("")
        self.gridLayout_2.addWidget(self.comboBox_termNum, 0, 1, 1, 1)
        self.pushButton_termNumApply = QtWidgets.QPushButton(MainWidgetAdtSettings)
        self.pushButton_termNumApply.setObjectName("pushButton_termNumApply")
        self.gridLayout_2.addWidget(self.pushButton_termNumApply, 1, 1, 1, 1)
        self.verticalLayout.addLayout(self.gridLayout_2)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout.addItem(spacerItem)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout.addItem(spacerItem1)
        self.horizontalLayout.addLayout(self.verticalLayout)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.gridLayout = QtWidgets.QGridLayout()
        self.gridLayout.setObjectName("gridLayout")
        self.label_lastestVersion = QtWidgets.QLabel(MainWidgetAdtSettings)
        self.label_lastestVersion.setText("")
        self.label_lastestVersion.setObjectName("label_lastestVersion")
        self.gridLayout.addWidget(self.label_lastestVersion, 1, 1, 1, 1)
        self.label_downloadProg = QtWidgets.QLabel(MainWidgetAdtSettings)
        self.label_downloadProg.setObjectName("label_downloadProg")
        self.gridLayout.addWidget(self.label_downloadProg, 2, 0, 1, 1)
        self.label_currentVersionTitle = QtWidgets.QLabel(MainWidgetAdtSettings)
        self.label_currentVersionTitle.setObjectName("label_currentVersionTitle")
        self.gridLayout.addWidget(self.label_currentVersionTitle, 0, 0, 1, 1)
        self.label_currentVersion = QtWidgets.QLabel(MainWidgetAdtSettings)
        self.label_currentVersion.setText("")
        self.label_currentVersion.setObjectName("label_currentVersion")
        self.gridLayout.addWidget(self.label_currentVersion, 0, 1, 1, 1)
        self.label_lastestVersionTitle = QtWidgets.QLabel(MainWidgetAdtSettings)
        self.label_lastestVersionTitle.setObjectName("label_lastestVersionTitle")
        self.gridLayout.addWidget(self.label_lastestVersionTitle, 1, 0, 1, 1)
        self.progressBar_download = QtWidgets.QProgressBar(MainWidgetAdtSettings)
        self.progressBar_download.setProperty("value", 0)
        self.progressBar_download.setObjectName("progressBar_download")
        self.gridLayout.addWidget(self.progressBar_download, 2, 1, 1, 1)
        self.label_extraInfo = QtWidgets.QLabel(MainWidgetAdtSettings)
        self.label_extraInfo.setText("")
        self.label_extraInfo.setObjectName("label_extraInfo")
        self.gridLayout.addWidget(self.label_extraInfo, 3, 1, 1, 1)
        self.pushButton_download = QtWidgets.QPushButton(MainWidgetAdtSettings)
        self.pushButton_download.setObjectName("pushButton_download")
        self.gridLayout.addWidget(self.pushButton_download, 3, 0, 1, 1)
        self.verticalLayout_2.addLayout(self.gridLayout)
        spacerItem2 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.verticalLayout_2.addItem(spacerItem2)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.verticalLayout_2.addItem(spacerItem3)
        self.horizontalLayout.addLayout(self.verticalLayout_2)

        self.retranslateUi(MainWidgetAdtSettings)
        QtCore.QMetaObject.connectSlotsByName(MainWidgetAdtSettings)

    def retranslateUi(self, MainWidgetAdtSettings):
        _translate = QtCore.QCoreApplication.translate
        self.label_termNum.setText(_translate("MainWidgetAdtSettings", "Terminal 數量："))
        self.comboBox_termNum.setItemText(0, _translate("MainWidgetAdtSettings", "1"))
        self.comboBox_termNum.setItemText(1, _translate("MainWidgetAdtSettings", "2"))
        self.pushButton_termNumApply.setText(_translate("MainWidgetAdtSettings", "應用"))
        self.label_downloadProg.setText(_translate("MainWidgetAdtSettings", "下載進度："))
        self.label_currentVersionTitle.setText(_translate("MainWidgetAdtSettings", "當前版本："))
        self.label_lastestVersionTitle.setText(_translate("MainWidgetAdtSettings", "最新版本："))
        self.pushButton_download.setText(_translate("MainWidgetAdtSettings", "下載最新壓縮檔"))
