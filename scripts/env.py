from configparser import ConfigParser
import os

__all__ = ['ENV']

class Env(object):
    """docstring for Env."""

    class _Gitlab(object):
        pid = str()
        pac = str() # personal access token

    class _GD(object):
        """para for google driver"""
        _cred_file = str()
        _pickle_file = str()
        folder_id = str()
        creds = object()

    gitlab = _Gitlab()

    def __init__(self, file):
        super(Env, self).__init__()
        self.env_paser(file)
    
    def env_paser(self, file):
        cfg = ConfigParser()
        cfg.read(file)
        self.gitlab.pid = cfg['gitlab']['pid']
        self.gitlab.pac = cfg['gitlab']['pac']

ENV = Env('env/.env')

